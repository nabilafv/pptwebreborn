from django.urls import path
from .views import index, savedStatus, profile

urlpatterns = [
	path('', index, name='home'),
	path('savedStatus/', savedStatus, name='savedStatus'),
	path('profile/', profile, name='profile'),
]