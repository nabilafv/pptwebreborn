from django import forms

class Status_Form(forms.Form):
    attrs = {'class': 'form-control'}
    status = forms.CharField(label='Status:', required=True, max_length=300, widget=forms.TextInput(attrs=attrs))