from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import savedStatus, question, index, profile
from .models import Status
from .forms import Status_Form


# Create your tests here.
class PptWebRebornTest(TestCase):
    def test_pptwebreborn_url_is_exist(self):
        response = Client().get('/pptwebreborn/')
        self.assertEqual(response.status_code, 200)

    def test_pptwebreborn_profile_url_is_exist(self):
        response = Client().get('/pptwebreborn/profile/')
        self.assertEqual(response.status_code, 200)

    def test_pptwebreborn_savedStatus_url_is_exist(self):
        response = Client().get('/pptwebreborn/savedStatus/')
        self.assertEqual(response.status_code, 302)

    def test_pptwebreborn_using_pptwebreborn_template(self):
        response = Client().get('/pptwebreborn/')
        self.assertTemplateUsed(response, 'pptwebreborn.html')

    def test_pptwebreborn_profile_using_profile_func(self):
        found = resolve('/pptwebreborn/profile/')
        self.assertEqual(found.func, profile)

    def test_pptwebreborn_using_index_func(self):
        found = resolve('/pptwebreborn/')
        self.assertEqual(found.func, index)

    def test_pptwebreborn_savedStatus_using_savedStatus_func(self):
        found = resolve('/pptwebreborn/savedStatus/')
        self.assertEqual(found.func, savedStatus)

    def test_question(self):
        self.assertIsNotNone(question)
        self.assertTrue(len(question) >= 15)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_status = Status.objects.create(status='having fun with lab 6 ppw')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="form-control', form.as_p())
        self.assertIn('id="id_status"', form.as_p())
        self.assertIn('<label for="id_status">Status:</label>', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_pptwebreborn_post_success_and_render_the_result(self):
        response_post = Client().post('/pptwebreborn/savedStatus', {'status': 'hehehe'})
        self.assertEqual(response_post.status_code, 200)