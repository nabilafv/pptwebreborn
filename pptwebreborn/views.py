from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

response = {}
question = "Hello, Apa kabar?"

# Create your views here.
def index(request):
    response['question'] = question
    response['status_form'] = Status_Form
    allStatus = Status.objects.all().values()
    response['allStatus'] = allStatus
    return render(request, 'pptwebreborn.html', response)

def profile(request):
    return render(request, 'profile.html', response)

def savedStatus(request):
    form = Status_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        status = Status(status=response['status'])
        status.save()
        allStatus = Status.objects.all().values()
        response['allStatus'] = allStatus
        return render(request, 'pptwebreborn.html', response)
    else:
        return HttpResponseRedirect('/pptwebreborn/')